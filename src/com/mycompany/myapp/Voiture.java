/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

/**
 *
 * @author Lenovo
 */
public class Voiture {
    
    
    private String serie ; 
    private String marque ; 
    private String couleur ; 
    private String model ; 
    private String dateEntretient ;
     private String image ; 

    public Voiture() {
    }

    public Voiture(String serie, String marque, String couleur, String model, String dateEntretient) {
        this.serie = serie;
        this.marque = marque;
        this.couleur = couleur;
        this.model = model;
        this.dateEntretient = dateEntretient;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    
    
    
    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDateEntretient() {
        return dateEntretient;
    }

    public void setDateEntretient(String dateEntretient) {
        this.dateEntretient = dateEntretient;
    }

    @Override
    public String toString() {
        return "Voiture{" + "serie=" + serie + ", marque=" + marque + ", couleur=" + couleur + ", model=" + model + ", dateEntretient=" + dateEntretient + '}';
    }
   
    
    
    
}
