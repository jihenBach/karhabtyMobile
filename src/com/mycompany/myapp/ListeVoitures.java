/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.ToastBar;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.notifications.LocalNotification;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import java.util.List;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
public class ListeVoitures {
    
    
    
        ConnectionRequest con = new ConnectionRequest();
       
        Form f = new Form("Liste des voitures",new BorderLayout());
        
  
        public ListeVoitures(Resources theme) throws JSONException{
             f.setUIID("back");
            
             
             
             con.setUrl("http://localhost/projetMobile/selectVoiture.php");
             Container k = new Container(new BoxLayout(BoxLayout.Y_AXIS));

             
             con.addResponseListener(new ActionListener<NetworkEvent>() {

                   @Override
                   public void actionPerformed(NetworkEvent evt) {
                       try {
                         ArrayList<Voiture> list = new  ArrayList<>();
                            list = getListVoiture(new String(con.getResponseData()));
                           for (int i = 0; i < list.size(); i++) {
                            //   System.err.println(list.get(i).getMarque());
                               Container c = new Container(new BoxLayout(BoxLayout.X_AXIS));
                               
                                Image im = FontImage.createMaterial(FontImage.MATERIAL_DELETE, UIManager.getInstance().getComponentStyle("Command"));
                                Image im1 = FontImage.createMaterial(FontImage.MATERIAL_EDIT, UIManager.getInstance().getComponentStyle("Command"));
                               Image im2 = FontImage.createMaterial(FontImage.MATERIAL_COMPUTER, UIManager.getInstance().getComponentStyle("Command"));

                                Button btnSupprimer = new Button(im);
                               Button btnModifier = new Button(im1);
                               Button btnShow = new Button(im2);
                               c.addComponent(btnSupprimer);
                                 
                               
                                String a = list.get(i).getSerie();
                                Voiture v = list.get(i);
                                
                             
                                  
                                Date date = new Date();
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                    String format = formatter.format(date);
                                  
                                  
                             Font materialFont = FontImage.getMaterialDesignFont();
                             FontImage fntImage = FontImage.createFixed("\uE855", materialFont, 0xff0000, 80, 80);
                           
                             
                             if(list.get(i).getDateEntretient().equals(format))
                             {
    
                                ToastBar.Status status = ToastBar.getInstance().createStatus();
                                status.setMessage("Vous avez un entretient aujourd'hui");
                                status.setIcon(fntImage);                            
                                status.show();
                          
                            }
          
                                ////
                                
                                                             

                               c.addComponent(btnModifier);
                               c.addComponent(btnShow);
                                Label l = new Label(list.get(i).getMarque());
                                l.setUIID("fontList");
                               SwipeableContainer sc = new SwipeableContainer(c, l);
                              sc.setUIID("backSwip");
                               k.addComponent(sc);
                               
                               ///btn Supprimer 
                               
                                btnSupprimer.addActionListener(new ActionListener() {
                                
                                    @Override
                                    public void actionPerformed(ActionEvent evt) {




                                            ConnectionRequest req = new ConnectionRequest(); //demande de connection
                                            req.setUrl("http://localhost/projetMobile/DeleteVoiture.php?serie=" + a + ""); //specifié url

                                            req.addResponseListener(new ActionListener<NetworkEvent>() { //attendre rep du serveur

                                                @Override
                                                public void actionPerformed(NetworkEvent evt) {
                                                    byte[] data = (byte[]) evt.getMetaData(); //recuperer les donné
                                                    String s = new String(data);

                                                    if (s.equals("success")) {
                                                        Dialog.show("Confirmation", "vous avez supprimer "+a, "Ok", null);
                                                         f.refreshTheme();
                                                    }
                                                }


                                            });

                                            NetworkManager.getInstance().addToQueue(req); //execution req // synglotent // addToque => executé par ordre



                                    }
                                });
                                
                                ///////btn show
                                
                                btnShow.addActionListener(new ActionListener() {
                                  
                                   @Override
                                   public void actionPerformed(ActionEvent evt) {
                                       
                                     
                                           detailVoiture d = new  detailVoiture(theme, v);
                                           d.getF().show();
                                      
                                   }
                                    
                                    });
                                
                                
                                ///btn Modifier 
                                
                                 btnModifier.addActionListener(new ActionListener() {
                                
                                    @Override
                                    public void actionPerformed(ActionEvent evt) {



                                         modifierVoitureForm v1 = new modifierVoitureForm(theme,v);
                                         
                                          v1.getF().show();





                                    }
                                });
                                 
                                 
                           }
                       
                           f.refreshTheme();
                           
                           
                           
                           
                           
                       } catch (JSONException ex) {
                       }

                   }
               });
            
        
            
               NetworkManager.getInstance().addToQueue(con);

            f.addComponent(BorderLayout.CENTER,k);
            
            
         
            Font materialFont = FontImage.getMaterialDesignFont();
            FontImage fntImage = FontImage.createFixed("\uE145", materialFont, 0xFFFFFF, 40, 40);
            
    Image im = FontImage.createMaterial(FontImage.MATERIAL_ADD, UIManager.getInstance().getComponentStyle("Command"));

            
                f.getToolbar().addCommandToRightBar("",fntImage , (ev) -> {
            
       
       ajoutVoitureForm v = new ajoutVoitureForm(theme);
       v.getF().show();
  
                });
            
                     Personne p =new  Personne();
                     p.setNom("jihen");
                     p.setId(9);
               f.getToolbar().setBackCommand(new Command("Back"){
              
                 @Override
               public void actionPerformed(ActionEvent evt){
                    HomeForm f = new HomeForm(theme, p);
                    f.getF().show();
               }
               
           });
            
        }

        
        
        
        
        
        
/*
 public void onEntered(String id) {
        if(Display.getInstance().isMinimized()) {
            Display.getInstance().callSerially(() -> {
                Dialog.show("Welcome", "Thanks for arriving", "OK", null);
            });
        } else {
            LocalNotification ln = new LocalNotification();
            ln.setId("LnMessage");
            ln.setAlertTitle("Welcome");
            ln.setAlertBody("Thanks for arriving!");
            Display.getInstance().scheduleLocalNotification(ln, 10, LocalNotification.REPEAT_NONE);
        }
    }    
 
*/
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
 public void show(){
     
      f.show(); 
 }
    
     
   
     public ArrayList<Voiture> getListVoiture(String json) throws JSONException {
        ArrayList<Voiture> listVoiture = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

           Map<String, Object> voiture = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    
                 //   System.out.println(voiture.get("voiture"));
                    
                    List <Map<String, Object>> list = (List<Map<String, Object>>) voiture.get("voiture");
                    
                    
                    JSONObject obj = new JSONObject(voiture);
                    
                    JSONArray array = obj.getJSONArray("voiture");
                    for(int i = 0 ; i < array.length() ; i++){
                        Voiture c = new Voiture();
                        String marque = array.getJSONObject(i).getString("marque") ;
                       String serie =  array.getJSONObject(i).getString("serie") ;
                       String couleur =  array.getJSONObject(i).getString("couleur") ;
                       String dateE =  array.getJSONObject(i).getString("dateEntretient") ;
                       String model =  array.getJSONObject(i).getString("model") ;
                       String image =  array.getJSONObject(i).getString("image") ;

                        c.setMarque(marque);
                        c.setSerie(serie);
                        c.setCouleur(couleur);
                        c.setDateEntretient(dateE);
                        c.setModel(model);   
                        c.setImage(image);
                       
                     listVoiture.add(c);
                   

            }

        } catch (IOException ex) {
         }
        return listVoiture;

    }
    
  
     
     public void onEntered(String id) {
        if(Display.getInstance().isMinimized()) {
            Display.getInstance().callSerially(() -> {
                Dialog.show("Welcome  aaaa" , "Thanks for arriving", "OK", null);
            });
        } else {
            LocalNotification ln = new LocalNotification();
            ln.setId("LnMessage");
            ln.setAlertTitle("Welcome");
            ln.setAlertBody("Thanks for arriving!");
            Display.getInstance().scheduleLocalNotification(ln, 10, LocalNotification.REPEAT_NONE);
        }
    }     
     
     
}


