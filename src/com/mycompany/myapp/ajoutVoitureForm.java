
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import org.json.JSONException;


public class ajoutVoitureForm {
    
    
    
        Form f = new Form("Ajout voiture",new BorderLayout());
        
          public ajoutVoitureForm(Resources theme){
              
                f.setUIID("back");
               // ajout image lfou9 fil west
         Container north = new Container(new FlowLayout(Component.CENTER));
    
    
                
               f.getToolbar().setBackCommand(new Command("Back"){
              
                 @Override
               public void actionPerformed(ActionEvent evt){
                     try {
                         ListeVoitures f = new ListeVoitures(theme);
                         f.getF().show();
                     } catch (JSONException ex) {
                         
                     }
               }
               
           });
     

       
        f.addComponent(BorderLayout.NORTH,north);
        
        /// ajout coordonné fil west
        
        
          Container centerWrapper = new Container(new BorderLayout());
        Container center =new Container(new BoxLayout(BoxLayout.Y_AXIS));
       
        TextField tfSerie = new TextField("", "Serie");
        TextField tfMarque = new TextField("", "Marque");
        TextField tfModel = new TextField("", "Model");
      
        Picker datePicker = new Picker();
        datePicker.setType(Display.PICKER_TYPE_DATE);

        Picker stringPicker = new Picker();
        stringPicker.setType(Display.PICKER_TYPE_STRINGS);


        center.addComponent(tfSerie);
        center.addComponent(tfMarque);
        center.addComponent(tfModel);
      
   
        center.add(datePicker);
         center.add(stringPicker);
         
        stringPicker.setStrings("Rouge", "Noir", "Blanc", "Gris", "Bleu");
        stringPicker.setSelectedString("Blanc");

        centerWrapper.addComponent(BorderLayout.NORTH, center);
        
        f.addComponent(BorderLayout.CENTER, centerWrapper);
        
        
        
         Button btnajout = new Button("Ajouter");
         btnajout.setUIID("btnVoiture");
        
        btnajout.setTextPosition(Component.LEFT);
        
        f.addComponent(BorderLayout.SOUTH , btnajout);

           btnajout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                ConnectionRequest req = new ConnectionRequest(); //demande de connection
              req.setUrl("http://localhost/projetMobile/insertVoiture.php?serie=" + tfSerie.getText() + "&marque=" + tfMarque.getText() + "&model=" + tfModel.getText()  + "&couleur=" + stringPicker.getText() + "&dateEntretient=" + datePicker.getText() + ""); //specifié url

                req.addResponseListener(new ActionListener<NetworkEvent>() { //attendre rep du serveur

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        try {
                            byte[] data = (byte[]) evt.getMetaData(); //recuperer les donné
                            String s = new String(data);
                            
                            if (s.equals("success")) {
                                Dialog.show("Confirmation", "Voiture ajoutée avec success ", "Ok", null);
                                
                            }
                            
                            ListeVoitures lv = new ListeVoitures(theme);
                            lv.getF().show();
                        } catch (JSONException ex) {
                        }
                    }
                    
                    
                });
                
                NetworkManager.getInstance().addToQueue(req); //execution req // synglotent // addToque => executé par ordre
                

            }
        });

           
           
              
              
              
          }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
          
          
          
          
    
}
