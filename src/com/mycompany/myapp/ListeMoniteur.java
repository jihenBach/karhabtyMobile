/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.notifications.LocalNotification;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ListeMoniteur {
    

    
    
    
        ConnectionRequest con = new ConnectionRequest();
        Form f = new Form("Liste des moniteurs",new BorderLayout());
        
  
        public ListeMoniteur(Resources theme ,Personne gerant) throws JSONException{
            f.setUIID("route");
            
            con.setUrl("http://localhost/projetMobile/selectMoniteur.php?idGerant=" + gerant.getId() + "");
            Container k = new Container(new BoxLayout(BoxLayout.Y_AXIS));

             
            con.addResponseListener(new ActionListener<NetworkEvent>() {

                   @Override
                   public void actionPerformed(NetworkEvent evt) {
                       try {
                         ArrayList<Personne> list = new  ArrayList<>();
                            list = getListPersonne(new String(con.getResponseData()));
                           for (int i = 0; i < list.size(); i++) 
                           {
                             
                               Container c = new Container(new BoxLayout(BoxLayout.X_AXIS));
                               
                               Image im = FontImage.createMaterial(FontImage.MATERIAL_DELETE, UIManager.getInstance().getComponentStyle("Command"));
                               Image im1 = FontImage.createMaterial(FontImage.MATERIAL_EDIT, UIManager.getInstance().getComponentStyle("Command"));
                               Image im2 = FontImage.createMaterial(FontImage.MATERIAL_COMPUTER, UIManager.getInstance().getComponentStyle("Command"));

                               Button btnSupprimer = new Button(im);
                               Button btnModifier = new Button(im1);
                               Button btnShow = new Button(im2);
                               c.addComponent(btnSupprimer);
                                 
                              String b = list.get(i).getNom();
                               int a = list.get(i).getId();
                               Personne v = list.get(i);

                               c.addComponent(btnModifier);
                               c.addComponent(btnShow);
                               
                               
                               Label l = new Label(list.get(i).getNom()+" "+ list.get(i).getPrenom());
                                
                                l.setUIID("fontList");
                                
                               SwipeableContainer sc = new SwipeableContainer(c, l );
                               
                              sc.setUIID("backSwip");
                               
                               

                             
                              
                               k.addComponent(sc);
                               
                              ///////////////////btn Supprimer 
                               
                                btnSupprimer.addActionListener(new ActionListener() {
                                
                                    @Override
                                    public void actionPerformed(ActionEvent evt) {

                                            ConnectionRequest req = new ConnectionRequest(); //demande de connection
                                            req.setUrl("http://localhost/projetMobile/DeleteMoniteur.php?id=" + a + ""); //specifié url

                                            req.addResponseListener(new ActionListener<NetworkEvent>() { //attendre rep du serveur

                                                @Override
                                                public void actionPerformed(NetworkEvent evt) {
                                                    byte[] data = (byte[]) evt.getMetaData(); //recuperer les donné
                                                    String s = new String(data);

                                                    if (s.equals("success")) {
                                                        Dialog.show("Confirmation", "vous avez supprimer "+b, "Ok", null);
                                                         f.refreshTheme();
                                                    }
                                                }


                                            });

                                            NetworkManager.getInstance().addToQueue(req); //execution req // synglotent // addToque => executé par ordre

                                    }
                                });
                                
                                        ///////////////////btn show 
                               
                              btnShow.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent evt) {
                                            Dialog d = new Dialog("detail Moniteur");
                                          
                                            TextArea popupBody = new TextArea("Cin : "+v.getCin()+"\n"+ "Nom : " + v.getNom()+"\n Prenom : "+v.getPrenom()+" \n N° Tel : "+v.getNumTel()+" \n Email : "+v.getEmail());
                                            popupBody.setUIID("PopupBody");
                                            popupBody.setEditable(false);
                                        
                                            d.setLayout(new BorderLayout());
                                            d.add(BorderLayout.CENTER, popupBody);
                                         
                                           
                                            d.showPopupDialog(btnShow);
                                        }
                                    });
                                ///////////////////////////btn Modifier 
                                
                                 btnModifier.addActionListener(new ActionListener()
                                 {
                                
                                    @Override
                                    public void actionPerformed(ActionEvent evt) 
                                    {
                                          modifierMoniteurForm v1 = new modifierMoniteurForm(theme,v);
                                          v1.getF().show();
                                    }
                                });
                                 
                                 
                                 
                                 
                                 
                          }
                       
                           f.refreshTheme();
                           
                       } catch (JSONException ex) {
                       }

                   }
               });
            
            
            
            
               NetworkManager.getInstance().addToQueue(con);

            f.addComponent(BorderLayout.CENTER,k);
            
            Font materialFont = FontImage.getMaterialDesignFont();
            FontImage fntImage = FontImage.createFixed("\uE145", materialFont, 0xFFFFFF, 40, 40);
            
          //  Image im = FontImage.createMaterial(FontImage.MATERIAL_ADD, UIManager.getInstance().getComponentStyle("Command"));
            f.getToolbar().addCommandToRightBar("",fntImage , (ev) -> 
            {
                ajouMoniteur v = new ajouMoniteur(theme, gerant);
                v.getF().show();
  
                });
           
            Image im1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, UIManager.getInstance().getComponentStyle("Command"));
            
                
           
                
                  Personne p =new  Personne();
                  p.setNom("jihen");
                  p.setId(9);
                  f.getToolbar().setBackCommand(new Command("Back"){
              
                 @Override
               public void actionPerformed(ActionEvent evt){
                    HomeForm f = new HomeForm(theme, p);
                    f.getF().show();
               }
               
           });
        }



 
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
 public void show(){
     
      f.show(); 
 }
    
     
   
     public ArrayList<Personne> getListPersonne(String json) throws JSONException {
        ArrayList<Personne> listPersonne = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

           Map<String, Object> utilisateur = j.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    
                  ///  System.out.println(utilisateur.get("utilisateur"));
                    
                    List <Map<String, Object>> list = (List<Map<String, Object>>) utilisateur.get("utilisateur");
                    
                    
                    JSONObject obj = new JSONObject(utilisateur);
        
                    JSONArray array = obj.getJSONArray("utilisateur");
                    for(int i = 0 ; i < array.length() ; i++){
                        Personne c = new Personne();
                        String nom = array.getJSONObject(i).getString("nom") ;
                       String cin =  array.getJSONObject(i).getString("cin") ;
                       String prenom =  array.getJSONObject(i).getString("prenom") ;
                       String email =  array.getJSONObject(i).getString("email") ;
                       String numTel =  array.getJSONObject(i).getString("numTel") ;
      int id = array.getJSONObject(i).getInt("id") ;
                        c.setNom(nom);
                        c.setPrenom(prenom);
                        c.setCin(cin);
                        c.setEmail(email);
                        c.setNumTel(numTel); 
                         c.setId(id); 
                      
                     listPersonne.add(c);
                   

            }

        } catch (IOException ex) {
         }
        return listPersonne;

    }
    
    
}
