/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.capture.Capture;
import com.codename1.components.ImageViewer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import org.json.JSONException;

/**
 *
 * @author Lenovo
 */
public class modifierVoitureForm {
    

    
    
        Form f = new Form("Modifier voiture",new BorderLayout());
        
          public modifierVoitureForm(Resources theme , Voiture v){
              
                f.setUIID("back");
               // ajout image lfou9 fil west
         Container north = new Container(new FlowLayout(Component.CENTER));
            Personne p1 =new Personne();
            p1.setNom("Jihen");
            p1.setId(9);
               f.getToolbar().setBackCommand(new Command("Back"){
              
                 @Override
               public void actionPerformed(ActionEvent evt){
                     try {
                        ListeVoitures l = new ListeVoitures(theme);
                         l.getF().show();
                     } catch (JSONException ex) {
                         
                     }
               }
               
           });
        
      
        
        f.addComponent(BorderLayout.NORTH,north);
        
        /// ajout coordonné fil west
        
        
          Container centerWrapper = new Container(new BorderLayout());
        Container center =new Container(new BoxLayout(BoxLayout.Y_AXIS));
       
        TextField tfSerie = new TextField(v.getSerie(), "Serie");
        TextField tfMarque = new TextField(v.getMarque(), "Marque");
        TextField tfModel = new TextField(v.getModel(), "Model");
       // TextField tfdateE = new TextField("", "date Entretient");
       // TextField tfCouleur = new TextField("", "Couleur");
        
     Picker datePicker = new Picker();
datePicker.setType(Display.PICKER_TYPE_DATE);

Picker stringPicker = new Picker();
stringPicker.setType(Display.PICKER_TYPE_STRINGS);


        center.addComponent(tfSerie);
        center.addComponent(tfMarque);
        center.addComponent(tfModel);
      
        center.add(datePicker);
         center.add(stringPicker);
         
        stringPicker.setStrings("Rouge", "Noir", "Blanc", "Gris", "Bleu");
        stringPicker.setSelectedString(v.getCouleur());

       
       /* TextField password = new TextField();
        password.setConstraint(TextField.PASSWORD);
        center.addComponent(password);*/
        
        
        centerWrapper.addComponent(BorderLayout.NORTH, center);
        
        f.addComponent(BorderLayout.CENTER, centerWrapper);
        
        
        
         Button btnajout = new Button("Modifier");
        btnajout.setUIID("btnVoiture");
        
        btnajout.setTextPosition(Component.LEFT);
        
        f.addComponent(BorderLayout.SOUTH , btnajout);

           btnajout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                ConnectionRequest req = new ConnectionRequest(); //demande de connection
              req.setUrl("http://localhost/projetMobile/modifierVoiture.php?serie=" + tfSerie.getText() + "&marque=" + tfMarque.getText() + "&model=" + tfModel.getText()  + "&couleur=" + stringPicker.getText() + "&dateEntretient=" + datePicker.getText() + ""); //specifié url

                req.addResponseListener(new ActionListener<NetworkEvent>() { //attendre rep du serveur

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        byte[] data = (byte[]) evt.getMetaData(); //recuperer les donné
                        String s = new String(data);

                        if (s.equals("success")) {
                            Dialog.show("Confirmation", " Voiture modifié avec succes", "Ok", null);
                        }
                            
                        
                        
                    }
                    
                    
                });
                
                NetworkManager.getInstance().addToQueue(req); //execution req // synglotent // addToque => executé par ordre
                

            }
        });

           
           
              
              
              
          }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
          
          
          
          
    
}
