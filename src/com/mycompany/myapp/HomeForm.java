/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import org.json.JSONException;

/**
 *
 * @author Lenovo
 */
public class HomeForm {
    
     Form f = new Form("Home",new FlowLayout(Component.CENTER, Component.CENTER)) ;
      public HomeForm(Resources theme, Personne p){
          
         f.setUIID("home");
           Container center = new Container(new FlowLayout(Component.CENTER));
            
       
        
         Label lNom =new Label("welcome" + "  " +p.getNom());
       
         

         
       center.addComponent(lNom);
    
     
     f.addComponent(center);
   

         
         f.getToolbar().addCommandToSideMenu("Liste des voitures ", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    /* AboutForm about = new AboutForm(theme);
                    about.getF().show();*/
                    
                    ListeVoitures listeV = new ListeVoitures(theme);
                    listeV.getF().show();
                } catch (JSONException ex) {
                   
                }
          
            }
        });
          
               f.getToolbar().addCommandToSideMenu("Liste des moniteur ", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    /* AboutForm about = new AboutForm(theme);
                    about.getF().show();*/
                    
                    ListeMoniteur listeV = new ListeMoniteur(theme,p);
                    listeV.getF().show();
                } catch (JSONException ex) {
                   
                }
          
            }
        });
         
      }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
      
}
