/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.capture.Capture;
import com.codename1.components.ImageViewer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import org.json.JSONException;

/**
 *
 * @author Lenovo
 */
public class modifierMoniteurForm {
    

    

    
    
    
        Form f = new Form("Modifier Moniteur",new BorderLayout());
        
          public modifierMoniteurForm(Resources theme , Personne p){
              
               f.setUIID("route");
               // ajout image lfou9 fil west
         Container north = new Container(new FlowLayout(Component.CENTER));
        
          Personne p1 =new Personne();
          p1.setNom("Jihen");
          p1.setId(9);
               f.getToolbar().setBackCommand(new Command("Back"){
              
                 @Override
               public void actionPerformed(ActionEvent evt){
                     try {
                         ListeMoniteur l = new ListeMoniteur(theme, p1);
                         l.getF().show();
                     } catch (JSONException ex) {
                         
                     }
               }
               
           });
        
        
        
    
        
        f.addComponent(BorderLayout.NORTH,north);
        
        /// ajout coordonné fil west
        
        
          Container centerWrapper = new Container(new BorderLayout());
        Container center =new Container(new BoxLayout(BoxLayout.Y_AXIS));
       TextField tfCin = new TextField(p.getCin(), "cin");
        TextField tfNom = new TextField(p.getNom(), "Nom");
        TextField tfPrenom = new TextField(p.getPrenom(), "Prenom");
        
       TextField tfMail = new TextField(p.getEmail(), "email");
        TextField tfNumTel = new TextField(p.getNumTel(), "N°Tel");
        



        center.addComponent(tfCin);
        center.addComponent(tfNom);
        center.addComponent(tfPrenom);
        center.addComponent(tfMail);
        center.addComponent(tfNumTel);
     
       

       
       /* TextField password = new TextField();
        password.setConstraint(TextField.PASSWORD);
        center.addComponent(password);*/
        
        
        centerWrapper.addComponent(BorderLayout.NORTH, center);
        
        f.addComponent(BorderLayout.CENTER, centerWrapper);
        
        
        
         Button btnajout = new Button("Modifier");
         btnajout.setUIID("btnMoniteur");
      p.setIdGerant(9);
        
        btnajout.setTextPosition(Component.LEFT);
        
        f.addComponent(BorderLayout.SOUTH , btnajout);

           btnajout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                ConnectionRequest req = new ConnectionRequest(); //demande de connection
              req.setUrl("http://localhost/projetMobile/modifierMoniteur.php?id=" + p.getId() + "&cin=" + tfCin.getText() + "&nom=" + tfNom.getText() + "&prenom=" + tfPrenom.getText()  + "&email=" + tfMail.getText() + "&numTel=" + tfNumTel.getText() + ""); //specifié url

                req.addResponseListener(new ActionListener<NetworkEvent>() { //attendre rep du serveur

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        try {
                            byte[] data = (byte[]) evt.getMetaData(); //recuperer les donné
                            String s = new String(data);
                            
                            if (s.equals("success")) {
                                Dialog.show("Confirmation", "Moniteur modifié avec success", "Ok", null);
                                
                            }
                            Personne p2 = new Personne();
                            p2.setId(p.getIdGerant());
                            ListeMoniteur lv = new ListeMoniteur(theme , p2);
                            lv.getF().show();
                        } catch (JSONException ex) {
                        }
                    }
                    
                    
                });
                
                NetworkManager.getInstance().addToQueue(req); //execution req // synglotent // addToque => executé par ordre
                

            }
        });

           
           
              
              
              
          }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
          
          
          
          
    
}
